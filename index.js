import home from "./src/pages/home.js";
import games from "./src/pages/game.js";
import greenlight from "./src/pages/greenlight.js";

var renderRoot = (element) => {
  const root = document.getElementById("root");
  root.innerHTML = element;
};

const initialLoad = () => {
  const url = new URL(window.location);
  const page = url.searchParams.get("page");
  router(page);
};

const router = (page) => {
  switch (page) {
    case "home":
      renderRoot(home());
      break;
    case "games":
      renderRoot(games());
      break;
    case "greenlight":
      renderRoot(greenlight());
      break;
    default:
      renderRoot(home());
      break;
  }
};

window.addEventListener("DOMContentLoaded", () => {
  initialLoad();

  window.navigation.addEventListener("navigate", (event) => {
    const url = new URL(event.destination.url);
    const page = url.searchParams.get("page");
    router(page);
  });

  document
    .getElementById("hamburger-menu-btn")
    .addEventListener("click", (e) => {
      const mobileMenu = document.getElementsByClassName("mobile-menu")[0];
      mobileMenu.classList.toggle("hidden");
    });

  const requirementElement = document.getElementsByClassName(
    "system-require-tab-item"
  );

  Array.from(requirementElement).forEach((element) => {
    element.addEventListener("click", (e) => {
      const key = e.target.getAttribute("data-system-require-item");
      const contentElement = document.querySelectorAll("[data-tab-content]");
      const content = Array.from(contentElement);

      content.forEach((element) => {
        element.classList.add("hidden");
        if (element.getAttribute("data-tab-content") === key) {
          element.classList.remove("hidden");
        }
      });
    });
  });
});
