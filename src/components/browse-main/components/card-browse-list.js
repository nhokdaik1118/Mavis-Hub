
import { browseGameCardData } from "../../../stores/games.data.js";
import { highlightGame } from "../../main/highlight-games/components/item-highlight.js"

export const cardBrowseList =()=>{
    const mapData = browseGameCardData
    .map((item) => {
      return highlightGame({img: item.img, name: item.name, badges: item.badges, platforms: [], type: "browse"});
    }) 
    .join("");
    return `
                        <div class="list-games">
                            <ul class="gap-6 mb-6 list-games-wrapper">
                             ${mapData}
                                
                            </ul>
                        </div>`
}