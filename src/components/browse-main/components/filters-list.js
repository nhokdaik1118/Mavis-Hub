import { appIcon } from "../../../constants.js";
import { filtersOptionData } from "../../../stores/games.data.js"
import { itemFilters } from "./item-filters.js";


export const filtersList = () => {

    return filtersOptionData
        .map((item, index) => {

            const isLastItem = index === filtersOptionData.length - 1;

            const lastClass = isLastItem ? "last-filter-item" : "";

            return `
      
                       <div class=" filters-item ">

                           <div class="pb-5 filters-item-wrapper ${lastClass}">
                               <div class="row filter-category">
                                   <span> ${item.title}</span>
                                   ${appIcon.arrowTop}
                               </div>
                               <div class="pt-5 row flex-col gap-4 filters-content">
                                   
                                     ${item.description
                    .map((items) => itemFilters({ text: items.text, type: "browse" }))
                    .join("")}

                               </div>
                           </div>

                           

                       </div>
                   `;
        })
        .join("");
};