import { appIcon } from "../../../constants.js";
export const itemFilters =(props)=>{

    const {text,type} =props;

    const ClassName  = type ==="browse"? "browse-item" : "";

    return `
    <label class="pl-7 row filters-content-wrapper ${ClassName}"> ${text}

                                            <input type="checkbox">

                                            <span class="icon-tick">
                                                ${appIcon.mark}
                                            </span>

                                        </label>`
}