import { filtersList } from "./components/filters-list.js"
import { cardBrowseList } from "./components/card-browse-list.js"
import { appIcon } from "../../constants.js"

export const browseMain =() =>{
    return `      
    <main>
        <div class="container row flex-col product-primary-browse">
            <div class="row flex-col product-browse-wrapper">

                <!-- aside -->
                <div class="row flex-col gap-6 aside">
                    <div class="row gap-2">
                        ${appIcon.arrowBack}
                        <span class="back-to-text">Back to Home</span>
                    </div>

                    <div class="filters-browse-mobile">
                        <button class="row filter-button gap-2 pr-4 pl-4">
                            <span>
                                ${appIcon.filter}
                            </span>
                            Filters (0)
                        </button>


                    </div>

                    <div class="gap-4 flex-col filters-browse">
                        <h5 class="mb-3 pb-6"> Filters
                            <div> 
                            ${appIcon.xMarkHidden}
                                </div>
                        </h5>

                        <div class="menu-filters row flex-col gap-5">
                        ${filtersList()}
                        </div>

                    </div>
                </div>

                <!-- list games -->
                <section class=" list-games-section">
                    <div class="container">
                        <div class="row flex-col gap-4">

                            <div class=" list-games">
                                ${cardBrowseList()}
                            </div>

                        </div>
                    </div>
                </section>

            </div>
        </div>
    </main>
`
}