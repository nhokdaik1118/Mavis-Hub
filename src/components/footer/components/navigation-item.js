import { appIcon } from "../../../constants.js";

export const navigationItem = (props) => {
  const {text,type} = props;

  return  type === "icon" 
    ? `<li class="row item-social-media">
          ${appIcon[text]}
       </li>`
    : `<div>${text}</div>`;

 ;
};
