import { footerData } from "../../../stores/games.data.js";
import { navigationItem } from "./navigation-item.js";

export const navigationList = () => {
  
  return footerData
    .map((col) => {
      const columnDirection = col.type === "icon" ? "" : "flex-col";

      return ` <div class="row gap-6 navigation">
              <div class="row flex-col gap-4">
                  <div class="nav-title">${col.title}</div>
                      <div class="row gap-3 list-nav ${columnDirection}">
                         ${col.items
          .map((item) => navigationItem({...item, type: col.type}))
          .join("")}
                      </div>
              </div>
          </div>`;
    })
    .join("");
};
