import { navigationList } from "./components/navigation.js"

export const footer = () =>{
    return `    <footer>
        <section class="footer-section">
            <div class="container">

                <div class="pt-10 pr-4 pl-4 pb-6 md-pt-10 md-pl-10 md-pr-10 md-pb-6 row flex-col gap-6 footer-content">

                    <div class="subscriber ">
                        <div class="subscriber-text mb-4">get the latest updates</div>

                        <div class="email-input">

                            <input class="button-input" type="text" placeholder="Email">

                            <span class=" arrow-email-input">
                                <svg viewBox="0 0 24 24" width="16" height="16" style="fill: currentcolor;">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M18.707 12.707a1 1 0 0 0 0-1.414l-4-4a1 1 0 1 0-1.414 1.414L15.586 11H6a1 1 0 1 0 0 2h9.586l-2.293 2.293a1 1 0 0 0 1.414 1.414l4-4Z"
                                        fill="currentColor"></path>
                                </svg>
                            </span>
                        </div>

                        <div class="mt-2 footer-description">By clicking you agree to our Privacy Policy term. You
                            always have the choice to unsubscribe
                            within every email you receive.</div>
                    </div>


                    ${navigationList()}


                </div>

                <div class="row flex-col gap-2 pt-6 pr-4 pb-10 pl-4 md-pt-6 md-pr-10 md-pl-10 md-pb-10 footer-credits">

                    <div class="row footer-copyright">© 2024 Sky Mavis</div>

                    <div class="row gap-6 footer-links">
                        <div class="link-item">Terms of Use</div>
                        <div class="link-item">Privacy Policy</div>
                    </div>
                </div>

            </div>
        </section>
    </footer>`
}