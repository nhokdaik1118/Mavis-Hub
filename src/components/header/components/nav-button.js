export const navButton = (props) => {
  const { text, icon, isActive } = props;

  return `
     <button class="row gap-3 pr-3 pl-3 justify-start ${
       isActive ? "mobile-menu-active" : ""
     }">
               ${icon}
            <p>${text}</p>
    </button>
        `;
};
