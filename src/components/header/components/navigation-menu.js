import { menuData } from "../../../stores/menu.data.js";

export const navigationMenu = () => {
  const mapData = menuData
    .map((item, index) => {
      const isActive = index === 0 ? "menu-item-active" : "";
      return `<div class="${isActive}"><a href="${item.url}"><button class='pl-4 pr-4'>${item.name}</button></a></div>`;
    })
    .join("");

  return `<div class="row menu-primary">${mapData}</div>`;
};
