import { navButton } from "./nav-button.js";
import { menuData } from "../../../stores/menu.data.js";
import { appIcon } from "../../../constants.js";

export const navigationMobileMenu = () => {
  const mapData = menuData
    .map((item, index) => {
      const isActive = index === 0 ? "mobile-menu-active" : "";
      return navButton({ text: item.name, icon: item.icon, isActive });
    })
    .join("");

  return ` <div class="row flex-col navbar-menu mobile-menu hidden">

            ${navButton({
              text: "Login",
              icon: appIcon.login,
            })}

        <div class="menu-item-line">

        </div>
        <div class="row flex-col menu-primary-mb">
        ${mapData}
        </div>`;
};
