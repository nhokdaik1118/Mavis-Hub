export const badgeItem = (props) => {
    const {text} = props;
          return ` 
          <div class="pt-1 pb-1 pr-2 pl-2 badge-item">
                                        <span class="badge-text">${text}</span>
                                    </div>`;
  };