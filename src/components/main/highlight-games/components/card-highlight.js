import { highlightGamesData } from "../../../../stores/games.data.js";
import { highlightGame } from "./item-highlight.js";

export const highlightGamesList = () => {
  const mapData = highlightGamesData
    .map((item) => {
      return highlightGame({ img: item.img, publisher: item.publisher, name: item.name, platforms: item.platforms, badges: item.badges, type: "highlight" });
    })
    .join("");

  return `
                 <div class="md-pr-10 md-pl-10 list-highlight-games">
                <ul class="gap-6 list-highlight-games-wrapper">
                    ${mapData}
                </ul>
            </div>  
    
    `;
};