import { appIcon } from "../../../../constants.js";
import { badgeItem } from "../../badges/components/badges.js";

export const highlightGame = (props) => {
    const { img, publisher, name, platforms, badges, type} = props;

    const sectionClassName  = type ==="browse"? "browse-item" : "";

    const isDisplayPublisher = publisher && platforms?.length > 0;


    const platformData = platforms?.map((item) => {
        return appIcon[item]
        
    }).join("");


    const badgesData = badges.map((item) => {
        return badgeItem({ text: item })
        
    }).join("");



    return `
    <li class="game-card ${sectionClassName}">
                        <div class="game-card-wrapper">

                            <div class="banner-games">
                                <img src="${img}"
                                    alt="banner-wf">
                            </div>

                            <div class="caption-games">

                              ${isDisplayPublisher ? `<div class="publisher-highlight-games">${publisher}</div>

                                <div class="row gap-2 platform-highlight-games">
                                     ${platformData}
                                </div>` : '' }

                                <div class="title-games">${name}</div>

                                <div class="row mt-2 gap-2 badge-games">
                                
                                    ${badgesData}
                                   
                                </div>

                            </div>
                        </div>
                    </li>
    `
}