import { highlightGamesList } from "./components/card-highlight"

export const highlightGames = () => {
    return `                <section class="highlight-games-section">
                    <div class="container">
                        <div class="row flex-col gap-4 highlight-games">

                            <div class="row gap-2 pr-4 pl-4 md-pr-10 md-pl-10 highlight-games-content">

                                <div class="row highlight-games-content-wrapper">
                                ${textTitlePrimary({text: titlePrimaryData.highlight})}     

                                    <div class="row navbar-browse-all">
                                        <div class="separator"></div>

                                        <div class="browse-all-button">
                                            <button>Browse all</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="row gap-3 slider-nav-button">
                                    <div>
                                        <button>
                                            <svg viewBox="0 0 24 24" width="24" height="24" style="fill: currentcolor;">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M14.625 6.22a1 1 0 0 1 .156 1.405L11.28 12l3.5 4.375a1 1 0 0 1-1.562 1.25l-4-5a1 1 0 0 1 0-1.25l4-5a1 1 0 0 1 1.406-.156Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </button>
                                    </div>
                                    <div>
                                        <button>
                                            <svg viewBox="0 0 24 24" width="24" height="24" style="fill: currentcolor;">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M9.375 6.22a1 1 0 0 0-.156 1.405L12.72 12l-3.5 4.375a1 1 0 0 0 1.562 1.25l4-5a1 1 0 0 0 0-1.25l-4-5a1 1 0 0 0-1.406-.156Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </button>
                                    </div>

                                </div>

                            </div>

                            ${highlightGamesList()}

                        </div>
                    </div>
                </section>`
}