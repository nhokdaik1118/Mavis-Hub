import { mostPlayedGamesList } from "../main/most-played-game/components/card-most-played-games.js"
import { textTitlePrimary } from "./title-primary/components/text-title-primary.js"
import { titlePrimaryData } from "../../stores/games.data.js"
import { highlightGamesList } from "./highlight-games/components/card-highlight.js"
import { newsAndEventsList } from "./news-and-events/components/card-news-and-events.js"
import { popularGenresList } from "./popular-genres/components/card-popular-genres.js"
import { sliderButton } from "./slider-nav-button/components/slider-button.js"

export const main = () => {
    return `
        <main>
        <div class="container row flex-col product-primary">
            <div class="row flex-col product-primary-wrapper">

                <!-- banner -->
                <section class="banner-section xl-pt-5 md-pr-10 md-pl-10">
                    <div class="row flex-col pl-4 pr-4 banner">

                        <div class="banner-wrapper">
                            <div class="banner-game-primary">
                                <img src="https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1715748292-homepagebanner1200_344.png"
                                    alt="banner-game-primary">
                            </div>

                            <div class="row flex-col banner-primary-content">

                                <div class="row flex-col banner-primary-text">
                                    PuffGo is Live on Ronin!
                                </div>

                                <div class="row action-buttons-game-banner">
                                    <div class="play-now-button">
                                        <button class="pr-4 pl-4 ">
                                        Play NOW
                                            
                                        </button>
                                    </div>


                                    <div class="pr-4 pl-4 read-more-link">
                                        <button class="row read-more-link-text">
                                            <div>Read more </div>
                                            <svg class="button-module_icon__-nXel" viewBox="0 0 24 24" width="16"
                                                height="16" style="fill: currentcolor;">
                                                <path
                                                    d="M4 7a3 3 0 0 1 3-3h3a1 1 0 1 0 0-2H7a5 5 0 0 0-5 5v10a5 5 0 0 0 5 5h10a5 5 0 0 0 5-5v-3a1 1 0 1 0-2 0v3a3 3 0 0 1-3 3H7a3 3 0 0 1-3-3V7Z"
                                                    fill="currentColor"></path>
                                                <path
                                                    d="M21.71 2.295a.995.995 0 0 1 .29.702V9a1 1 0 1 1-2 0V5.414l-7.293 7.293a1 1 0 0 1-1.414-1.414L18.586 4H15a1 1 0 1 1 0-2h6c.275 0 .524.111.705.29l.004.005Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </section>

                <!-- featured games -->

                <section class="featured-games-section">
                    <div class="container">
                        <div class="row gap-4 flex-col featured-games">

                            <!-- title -->
                            <div class="row pr-4 pl-4 md-pr-10 md-pl-10 featured-games-content">

                            ${textTitlePrimary({ text: titlePrimaryData.featured })}

                                <div class="row gap-3 slider-nav-button">
                                    <div>
                                        <button>
                                            <svg viewBox="0 0 24 24" width="24" height="24" style="fill: currentcolor;">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M14.625 6.22a1 1 0 0 1 .156 1.405L11.28 12l3.5 4.375a1 1 0 0 1-1.562 1.25l-4-5a1 1 0 0 1 0-1.25l4-5a1 1 0 0 1 1.406-.156Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </button>
                                    </div>
                                    <div>
                                        <button>
                                            <svg viewBox="0 0 24 24" width="24" height="24" style="fill: currentcolor;">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M9.375 6.22a1 1 0 0 0-.156 1.405L12.72 12l-3.5 4.375a1 1 0 0 0 1.562 1.25l4-5a1 1 0 0 0 0-1.25l-4-5a1 1 0 0 0-1.406-.156Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </button>
                                    </div>

                                </div>
                            </div>

                            <!-- video -->
                            <div class="pr-4 pl-4 md-pr-10 md-pl-10 xl-pr-10 xl-pl-10 game-promotion-card">

                                <div class="row flex-col xl-col-7 featured-video">
                                    <div class="row flex-col featured-video-wrapper">

                                        <iframe class="video-puffverse-featured" width="100%" height="194"
                                            src="https://www.youtube.com/embed/J19Ypn7PV6w?si=x666OBrFx1l4qu9K"
                                            title="YouTube video player" frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                            referrerpolicy="strict-origin-when-cross-origin" allowfullscreen>
                                        </iframe>
                                    </div>
                                </div>

                                <div class="xl-col-5 featured-video-content">
                                    <div class="video-content-wrapper">

                                        <img class="logo-puffgo"
                                            src="https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1713172466-puffgologo-192_192.png"
                                            alt="logo-puffgo">

                                        <div class="title-featured-game">PuffGo</div>

                                        <div class="mt-2 featured-game-description">
                                            PuffGo is an on-chain multiplayer royale party game that allows players to
                                            earn while having fun.
                                        </div>

                                        <div class="row gap-2 mt-6 mb-6">

                                            <div class="pt-1 pb-1 pr-2 pl-2 badge-item">
                                                <span class="badge-text">Early access}</span>
                                            </div>

                                            <div class="pt-1 pb-1 pr-2 pl-2 badge-item">
                                                <span class="badge-text">Battle Royale</span>
                                            </div>
                                        </div>

                                        <div class="row mb-2 action-panel-puffgo">
                                            <div class="button-play-puffgo pr-4 pl-4">
                                                <button>Play Now</button>
                                            </div>

                                            <div class="row">
                                                <div class="row gap-2 platform-icon">

                                                    <svg viewBox="0 0 24 24" width="20" height="20"
                                                        style="fill: currentcolor;">
                                                        <path
                                                            d="m14.975 3.019.96-1.732a.192.192 0 0 0-.075-.263.193.193 0 0 0-.263.076l-.97 1.75a6.541 6.541 0 0 0-5.253 0l-.97-1.75a.193.193 0 0 0-.34.187l.96 1.732a5.546 5.546 0 0 0-3.092 4.876h12.136a5.546 5.546 0 0 0-3.093-4.876ZM9.2 5.674a.507.507 0 1 1 0-1.013.507.507 0 0 1 0 1.013Zm5.602 0a.507.507 0 1 1 0-1.013.507.507 0 0 1 0 1.013ZM5.93 17.171A1.467 1.467 0 0 0 7.4 18.64h.973v3a1.36 1.36 0 1 0 2.721 0v-3h1.814v3a1.361 1.361 0 1 0 2.72 0v-3h.975a1.467 1.467 0 0 0 1.467-1.468V8.375H5.93v8.796Zm-1.867-9.03a1.362 1.362 0 0 0-1.36 1.361v5.669a1.36 1.36 0 1 0 2.72 0V9.502a1.362 1.362 0 0 0-1.36-1.36Zm15.872 0a1.362 1.362 0 0 0-1.36 1.361v5.669a1.36 1.36 0 1 0 2.72 0V9.502a1.362 1.362 0 0 0-1.36-1.36Z"
                                                            fill="currentColor"></path>
                                                    </svg>

                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="currentColor"
                                                        xmlns="http://www.w3.org/2000/svg" size="20">
                                                        <path
                                                            d="M11.6329 14.1831C12.041 13.7925 12.3999 13.1236 12.3999 12C12.3999 10.8764 12.041 10.2075 11.6329 9.81689C11.2084 9.41051 10.6204 9.2 9.99993 9.2C9.37942 9.2 8.79151 9.41051 8.36695 9.81689C7.95883 10.2075 7.59993 10.8764 7.59993 12C7.59993 13.1236 7.95883 13.7925 8.36695 14.1831C8.79151 14.5895 9.37942 14.8 9.99993 14.8C10.6204 14.8 11.2084 14.5895 11.6329 14.1831Z">
                                                        </path>
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                            d="M6 2C3.79086 2 2 3.79086 2 6V18C2 20.2091 3.79086 22 6 22H18C20.2091 22 22 20.2091 22 18V6C22 3.79086 20.2091 2 18 2H6ZM15.717 8.37576C16.0938 8.13075 16.5365 8 16.9893 8H17.2162C18.4461 8 19.5181 8.81648 19.8164 9.98033L18.666 10.2609C18.4997 9.61194 17.902 9.15669 17.2162 9.15669H16.9893C16.7706 9.15669 16.5568 9.21984 16.3748 9.33819L16.295 9.39011C16.0366 9.55809 15.8815 9.8409 15.8815 10.1437C15.8815 10.5522 16.1617 10.9101 16.5657 11.0176L18.3089 11.4813C19.445 11.7836 20.1621 12.8756 19.9685 14.0087C19.8478 14.715 19.3879 15.3224 18.7313 15.6427L18.5618 15.7254C18.0313 15.9841 17.4276 16.0629 16.8461 15.9495L16.6485 15.9109C15.8022 15.7458 15.0578 15.2595 14.579 14.559L14.4 14.2971L15.3866 13.6554L15.5657 13.9174C15.87 14.3627 16.3431 14.6718 16.8811 14.7767L17.0787 14.8153C17.4016 14.8783 17.7369 14.8345 18.0315 14.6908L18.201 14.6081C18.5183 14.4533 18.7405 14.1598 18.7988 13.8185C18.8924 13.271 18.5459 12.7433 17.9969 12.5973L16.2537 12.1335C15.3338 11.8888 14.6957 11.0738 14.6957 10.1437C14.6957 9.45416 15.049 8.81019 15.6372 8.42768L15.717 8.37576ZM4 16.0001V10.4999H5.2V16.0001H4ZM4.6 8C4.26863 8 4 8.26863 4 8.6C4 8.93137 4.26863 9.2 4.6 9.2C4.93137 9.2 5.2 8.93137 5.2 8.6C5.2 8.26863 4.93137 8 4.6 8ZM9.99993 16C11.7672 16 13.5999 14.8 13.5999 12C13.5999 9.2 11.7672 8 9.99993 8C8.23262 8 6.39993 9.2 6.39993 12C6.39993 14.8 8.23262 16 9.99993 16Z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>

                <!-- most played games -->
                <section class=" most-played-games-section">
                    <div class="container">
                        <div class="row flex-col gap-4 most-played-games">
                            <div class="row pr-4 pl-4 md-pr-10 md-pl-10 most-played-games-content">

                            ${textTitlePrimary({ text: titlePrimaryData.most })}

                                <div class="row gap-3 slider-nav-button">
                                    ${sliderButton()}

                                </div>
                            </div>

                             ${mostPlayedGamesList()}
                        
                        </div>
                    </div>

                </section>

                <!-- highlight games -->
                <section class="highlight-games-section">
                    <div class="container">
                        <div class="row flex-col gap-4 highlight-games">

                            <div class="row gap-2 pr-4 pl-4 md-pr-10 md-pl-10 highlight-games-content">

                                <div class="row highlight-games-content-wrapper">
                                ${textTitlePrimary({ text: titlePrimaryData.highlight })}

                                    <div class="row navbar-browse-all">
                                        <div class="separator"></div>

                                        <div class="browse-all-button">
                                            <button>Browse all</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="row gap-3 slider-nav-button">
                                    <div>
                                        <button>
                                            <svg viewBox="0 0 24 24" width="24" height="24" style="fill: currentcolor;">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M14.625 6.22a1 1 0 0 1 .156 1.405L11.28 12l3.5 4.375a1 1 0 0 1-1.562 1.25l-4-5a1 1 0 0 1 0-1.25l4-5a1 1 0 0 1 1.406-.156Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </button>
                                    </div>
                                    <div>
                                        <button>
                                            <svg viewBox="0 0 24 24" width="24" height="24" style="fill: currentcolor;">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M9.375 6.22a1 1 0 0 0-.156 1.405L12.72 12l-3.5 4.375a1 1 0 0 0 1.562 1.25l4-5a1 1 0 0 0 0-1.25l-4-5a1 1 0 0 0-1.406-.156Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </button>
                                    </div>

                                </div>

                            </div>

                            ${highlightGamesList()}

                        </div>
                    </div>
                </section>

                <!-- news and events -->
                <section class="news-and-events-section">
                    <div class="container">
                        <div class="row flex-col gap-2">
                            <div class="pr-4 pl-4 md-pr-10 md-pl-10 row news-and-events-content">
                            ${textTitlePrimary({ text: titlePrimaryData.news })}
                            </div>
                            ${newsAndEventsList()}
                          </div> 
                    </div>
                </section>

                <!-- popular genres -->
                <section>
                    <div class="container">

                        <div class="row flex-col gap-4">

                            <div class="row gap-2 pr-4 pl-4 md-pr-10 md-pl-10 popular-genres-content">

                                <div class="row popular-genres-content-wrapper">
                                ${textTitlePrimary({ text: titlePrimaryData.popular })}

                                    <div class="row navbar-browse-all">
                                        <div class="separator"></div>

                                        <div class="browse-all-button">
                                            <button>Browse all</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="row gap-3 slider-nav-button">
                                    <div>
                                        <button>
                                            <svg viewBox="0 0 24 24" width="24" height="24" style="fill: currentcolor;">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M14.625 6.22a1 1 0 0 1 .156 1.405L11.28 12l3.5 4.375a1 1 0 0 1-1.562 1.25l-4-5a1 1 0 0 1 0-1.25l4-5a1 1 0 0 1 1.406-.156Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </button>
                                    </div>
                                    <div>
                                        <button>
                                            <svg viewBox="0 0 24 24" width="24" height="24" style="fill: currentcolor;">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M9.375 6.22a1 1 0 0 0-.156 1.405L12.72 12l-3.5 4.375a1 1 0 0 0 1.562 1.25l4-5a1 1 0 0 0 0-1.25l-4-5a1 1 0 0 0-1.406-.156Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </button>
                                    </div>

                                </div>
                            </div>

                             ${popularGenresList()}

                        </div>
                    </div>
                </section>

                <!-- voting panel -->
                <section>
                    <div class="pr-4 pl-4 md-pr-10 md-pl-10 container">
                        <div class=" row flex-col gap-6 voting-panel">

                            <div class="mt-6 ml-4 voting-panel-logo">
                                <img src="https://hub.skymavis.com/assets/buba-mascot.png" alt="banner-buaba">
                            </div>

                            <div class="row voting-panel-items">
                                <div class="row flex-col gap-6 pr-4 pl-4 pb-10 voting-panel-items-wrapper">

                                    <div class="row flex-col gap-1">

                                        <div class="business-name">
                                            <img src="https://hub.skymavis.com/assets/event-banner-brand.svg"
                                                alt="banner-mavis-hub">
                                        </div>

                                        <div class="greenlight-title">
                                            Greenlight
                                        </div>

                                        <div class="voting-panel-description">
                                            Vote to choose the next games listed on Mavis Hub!
                                        </div>
                                    </div>

                                    <div class="voting-button">
                                        <button class="pr-4 pl-4"> Start voting now!
                                            <span> <svg class="button-module_icon__-nXel" viewBox="0 0 24 24" width="16"
                                                    height="16" style="fill: currentcolor;">
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                        d="M11.789 21H8.123a4 4 0 0 1-3.88-3.03L2.62 11.485A2 2 0 0 1 4.561 9H10L8.591 6.887C7.484 5.225 8.675 3 10.671 3H12l3.868 6.77a1 1 0 0 1 .132.496v8.199a1 1 0 0 1-.445.832l-1.547 1.031a4 4 0 0 1-2.22.672ZM20 8h-.5a2 2 0 0 0-2 2v9a2 2 0 0 0 2 2h.5a2 2 0 0 0 2-2v-9a2 2 0 0 0-2-2Z"
                                                        fill="currentColor"></path>
                                                </svg></span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    `
}
