import { mostPlayedGamesData } from "../../../../stores/games.data.js";
import { mostPlayedGame} from "./most-played-games-item.js";

export const mostPlayedGamesList = () => {
  const mapData = mostPlayedGamesData
  .map((item) => {
    return mostPlayedGame({ name: item.name, description: item.description, img: item.img });
  })
  .join("");

  return `
                        <div class="pr-4 pl-4 md-pr-10 md-pl-10 list-most-played-games">
                                <ul class="gap-6 list-most-played-games-wrapper">
                                  ${mapData}
                                </ul>
                            </div>
  
  `;
};