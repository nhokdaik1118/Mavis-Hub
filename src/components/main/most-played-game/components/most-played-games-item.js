export const mostPlayedGame= (props) => {
    const { name, description, img} = props;
  
    return `
          <li class="game-item">
                                        <div class="game-item-wrapper">
                                            <div class="game-item-img">
                                                <img src="${img}"
                                    alt="banner-game-primary">
                                            </div>

                                            <div>
                                                <div
                                                    class=" pr-6 pl-6 pb-6 row flex-col gap-1  most-played-section-caption">
                                                    <div class="most-played-game-title">${name}</div>
                                                    <div class="most-played-games-description">${description}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
          `;
  };