import { titlePrimaryData } from "../../../stores/games.data.js"
import {mostPlayedGamesList} from "./components/card-most-played-games.js"
import { textTitlePrimary } from "../title-primary/components/text-title-primary.js"

export const mostPlayedGames = () => {
    return `
    <section class="xl-mb-10 most-played-games-section">
    <div class="container">
        <div class="row flex-col gap-4 most-played-games">
            <div class="row pr-4 pl-4 md-pr-10 md-pl-10 most-played-games-content">

                ${textTitlePrimary({text: titlePrimaryData.featured})}

                <div class="row gap-3 slider-nav-button">
                ${sliderButton()}

                </div>
            </div>


            <div class="pr-4 pl-4 md-pr-10 md-pl-10 list-most-played-games">
                <ul class="gap-6 list-most-played-games-wrapper">

                    ${mostPlayedGamesList()}
                </ul>
            </div>
        </div>
    </div>

</section>
    `
}