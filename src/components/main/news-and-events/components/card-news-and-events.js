import { itemNewsAndEvens } from "./news-and-events-item.js";
import { newsAndEventsData } from "../../../../stores/games.data.js";

export const newsAndEventsList = () => {
    const mapData = newsAndEventsData
    .map((item) => {
      return itemNewsAndEvens({img: item.img,title: item.title, date: item.date });
    })
    .join("");
  
    return `
                 <div class="md-pr-10 md-pl-10">
                <ul class="gap-6 news-events-list">
                    ${mapData}

                </ul>
            </div>
    
    `;
  };