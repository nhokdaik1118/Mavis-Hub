

export const itemNewsAndEvens = (props) => {
    const {img,title,date} = props;
    return `
    <li class="news-and-events-item">
                        <div class="news-and-events-item-wrapper">
                            <div class="news-events-banner">
                                <img src="${img}">
                            </div>

                            <div class="news-and-events-caption">
                                <div class="news-and-events-title">${title}</div>
                                <div class="news-and-events-date">${date}</div>
                            </div>
                        </div>
                    </li>
    `

    }