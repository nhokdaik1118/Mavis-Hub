import { newsAndEventsList } from "./components/card-news.js"

export const newsAndEvents = () => {
    
    return `<section class="news-and-events-section">
                    <div class="container">
                        <div class="row flex-col gap-2">
                            <div class="pr-4 pl-4 md-pr-10 md-pl-10 row news-and-events-content">
                                <div class="title-primary">News & Events</div>
                            </div>

                           ${newsAndEventsList()}
                        </div>
                    </div>
                </section>`
}