import { popularGenresData } from "../../../../stores/games.data.js";
import { itemPopularGenres } from "./popular-genres-item.js";

export const popularGenresList = () =>{
    const mapData = popularGenresData
    .map((item) => {
      return itemPopularGenres({title: item.title, icon:item.icon});
    })
    .join("");
    return`<div class="md-pr-10 md-pl-10">
                <ul class="row gap-6 popular-genres-list">
                    ${mapData}
                </ul>
            </div>
    `
}