import { appIcon } from "../../../../constants.js";

export const itemPopularGenres = (props) =>{
    const {title,icon} = props;

    return `
     <li class="popular-genres-item">
                        <div class="row flex-col popular-genres-item-wrapper">
                            <div class="row flex-col gap-2 genres-icon">
                                ${appIcon[icon]}
                                <span class="genres-title">${title}</span>
                            </div>
                        </div>
                    </li>`
}