import { appIcon } from "../../../../constants.js";

export const sliderButton = () =>{

    return ` <div>
                        <button>
                            ${appIcon.arrowRight}
                        </button>
                    </div> 
                    <div>
                        <button>
                            ${appIcon.arrowLeft}
                        </button>
                    </div>`
}