import { sliderButton } from "./components/slider-button"

export const sliderNavButton = () =>{
    return `<div class="row gap-3 slider-nav-button">
                 ${sliderButton}
                </div>`
}