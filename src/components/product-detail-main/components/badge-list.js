import { badgeData } from "../../../stores/games.data.js";
import { badgeItem } from "../../main/badges/components/badges.js"

export const badgeList=()=>{

    const mapData = badgeData.map((item) => {
        return badgeItem({ text: item.badges })
        
    }).join("");
    
    return `
     <div class="row mt-2 gap-2 badge-games">
                                  ${mapData}  
                                </div>`
}