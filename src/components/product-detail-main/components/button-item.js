import { appIcon } from "../../../constants.js";

export const buttonDetailItem=(props)=>{
    const {title, icon} =props;

    const isClassName = title === "Install"? "call-to-action":"button-active";

    return icon? `<div class="row app-action-button ${isClassName}">
                                        <button class="row gap-2">
                                        ${appIcon[icon]}
                                           <span> ${title} </span> 
                                        </button>
                                    </div>` : `<div class="row app-action-button ${isClassName}">
                                        <button class="row gap-2">
                                           <span> ${title} </span> 
                                        </button>
                                    </div>`;
}