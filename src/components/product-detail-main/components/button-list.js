import { buttonDetailData } from "../../../stores/games.data.js"
import { buttonDetailItem } from "./button-item.js"

export const buttonList =()=>{
    const mapData =buttonDetailData.map((item)=>{
        return buttonDetailItem({title: item.title, icon:item.icon})
    }).join("");

    return `<div class="row flex-col gap-3 interactive-button">

    ${mapData}
                                </div>`
};