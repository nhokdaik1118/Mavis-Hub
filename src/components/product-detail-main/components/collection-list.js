import { collectionData} from "../../../stores/games.data.js"
export const collectionList =()=>{

    return collectionData.map((item)=>{
        return `<div class="collection-item">
                   <img src="${item.img}"
                                                alt="logo-axie">
                                        </div>`
    }).join("");
}