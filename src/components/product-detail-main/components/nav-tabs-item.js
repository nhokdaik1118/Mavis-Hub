export const navTabsItem = (props) => {
  const { text } = props;

  return `<div class="mr-2 pt-2 pb-2 pr-4 pl-4 tab-item system-require-tab-item" data-system-require-item="${text}"> ${text}
                                        </div>`;
};
