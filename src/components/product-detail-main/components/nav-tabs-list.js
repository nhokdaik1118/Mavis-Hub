import { navTabsData } from "../../../stores/games.data.js"
import { navTabsItem } from "./nav-tabs-item.js"

export const navTabsList=()=>{

    const mapData =navTabsData.map((item)=>{
       return navTabsItem({text:item.text})
    }).join("");
    return `
    <div class="row gap-3 tab-content">

                                      ${mapData}  

                                    </div>
                                 `

                                    
}