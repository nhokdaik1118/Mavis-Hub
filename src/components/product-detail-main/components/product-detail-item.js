import { appIcon } from "../../../constants.js";

export const productDetailItem =(props)=>{
    const {text,platforms} = props;

    const platformData = platforms?.map((item) => {
        return appIcon[item]
        
    }).join("");

    return text ? `<div>${text}</div>` : `
    <div class="row gap-2 platform-icon-details">
        ${platformData}
    </div>`;
};