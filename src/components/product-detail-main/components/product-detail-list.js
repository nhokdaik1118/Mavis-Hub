
import { productDetailData } from "../../../stores/games.data.js";
import { productDetailItem } from "./product-detail-item.js"

export const productDetailList=()=>{
    return productDetailData
    .map((item) => {
     

      return `<div class="row gap-2 product-details-item">
                                    
                                        <div class="product-detail-title">${item.title}</div>

                                        ${productDetailItem({text:item.text, platforms:item.platforms })}
                                    </div> `;
    })
    .join("");
};