import { systemRequirementsDetailData } from "../../../stores/games.data.js";
import { systemRequirementsItem } from "./system-requirement-item.js";


export const systemRequirementsDetail = (props) => {
  const { key,requirements } = props;

  const data = requirements
    .map((item) => {
      return `
    <div class="row flex-col gap-3 system-requirements-item" >
         <div class="system-requirements-title">${item.title}</div>
        ${Object.entries(item.specName)
          .map(([key, value]) => systemRequirementsItem({ key, value }))
          .join("")} 
    </div> `;
    })
    .join("");

  return `<div class="pt-4 pb-5 pr-5 pl-5 row gap-5 flex-col system-requirements" data-tab-content="${key}">${data}</div>`;
};




