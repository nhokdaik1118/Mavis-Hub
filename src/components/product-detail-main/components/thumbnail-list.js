import { thumbnailListDataDetail } from "../../../stores/games.data.js"

export const thumbnailList =() =>{
    return thumbnailListDataDetail.map((item)=>{
      return`                            
      
        <li class="thumbnail-item">
                <img src="${item.img}"
                    alt="banner-axie">
            </li>

       
`}).join("");
};