import { appIcon } from "../../constants.js"

export const fixedHeader =()=>{
    return ` <!-- fixed -->

        <div class="fixed-game-info row flex-col gap-4 xl-pt-6 xl-pb-4">

            <div class="row gap-2">
                ${appIcon.arrowBack}
                <span class="navigate-back-fixed">Back to Browse Games </span>
            </div>

            <div class="row floating-game-title xl-pb-4">
                <div class="product-content-fixed-wrapper row gap-4 ">

                    <div class="product-logo-fixed">
                        <img src="https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-game.png"
                            alt="logo-origins">
                    </div>

                    <span class="product-name-fixed row">Axie Infinity: Origins</span>
                </div>

                <div class="row game-controls gap-4">
                    <div class="game-controls-button">
                        <button>
                            ${appIcon.share}
                        </button>
                    </div>

                    <div class="game-controls-button">
                        <button>
                            ${appIcon.mobile}
                        </button>
                    </div>

                    <div class="game-controls-button">
                        <button>
                            Install
                        </button>
                    </div>
                </div>
            </div>
        </div>`
}