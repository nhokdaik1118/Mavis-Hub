import { thumbnailList } from "./components/thumbnail-list.js";
import { badgeList } from "./components/badge-list.js";
import { systemRequirementsDetail } from "./components/system-requirements-list.js";
import { appIcon } from "../../constants.js";
import { navTabsList } from "./components/nav-tabs-list.js";
import { productDetailList } from "./components/product-detail-list.js";
import { buttonList } from "./components/button-list.js";
import {collectionList} from "./components/collection-list.js";
import { fixedHeader } from "./fixed-header.js";
import { systemRequirementsDetailData } from "../../stores/games.data.js";

export const productDetailMain = () => {
  return `    
  <main>
        <div class="container row flex-col ">
        
        ${fixedHeader()}

         <!-- product -->
            <div class=" product-detail row flex-col gap-6">

                <div class="pt-6 pr-4 pl-4 md-pr-10 md-pl-10 row flex-col gap-6">

                    <div class="row gap-2">
                        ${appIcon.arrowBack}
                        <span class="navigate-back">Back to Browse Games </span>
                    </div>

                    <div class="product-content">

                        <div class="product-content-wrapper row gap-6">

                            <div class="product-logo">
                                <img src="https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-game.png"
                                    alt="logo-origins">
                            </div>

                            <span class="product-name mt-3 row">Axie Infinity: Origins</span>
                        </div>

                        <div class="mt-6 product-description">A PVP tactical card battler</div>

                    </div>

                </div>

                <section class="pb-6 xl-pb-10">
                    <div class="mr-4 ml-4 mb-4 xl-mr-10 xl-ml-10 xl-mb-6 nav-tabs">

                        <div class="row gap-3 tab-content">

                            <div class="mr-2 pt-2 pb-2 pr-4 pl-4 tab-item overview-tab-item">Overview</div>

                            <div class="row pt-2 pb-2 pr-4 pl-4 gap-2">
                                <div class="items-tab-item">
                                    Items
                                </div>
                                <span class="new-tab-item">
                                    New
                                </span>
                            </div>

                        </div>
                    </div>

                    <div class=" row flex-col gap-4 pr-4 pl-4 md-pl-10 md-pr-10 xl-mb-5">

                        <div class="row flex-col gap-4 product-display">

                        <div class="row flex-col gap-4 thumbnail-list xl-col-9">

                            <div class="full-view-thumbnail">
                                <img src="https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-banner-key-art.png"
                                    alt="banner-axie">
                            </div>
                            <ul class="row gap-4 thumbnail-list-wrapper">
                            ${thumbnailList()}
                            </ul>

                        </div>
                    
                            <div class="row mt-4 flex-col gap-4 app-details-container xl-col-3">
                                
                                ${buttonList()}

                                <div class="row flex-col gap-4 product-details">

                                    <div class="product-item-head">
                                        Details
                                    </div>

                                    ${badgeList()}

                                    ${productDetailList()}

                                    <div class="separator-product-detail">

                                    </div>

                                    <div class="row flex-col gap-2">
                                        <div class="row gap-2 product-detail-title">
                                            <span>COLLECTIBLES</span>

                                            <span>
                                                ${appIcon.questionMark}
                                            </span>
                                        </div>

                                        <div class="row gap-2">
                                        ${collectionList()}
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>

                        <div class="row flex-col gap-3 mt-4 description-section xl-mt-6 xl-col-9">

                            <div class="pt-2 pb-2 title-primary">
                                Description
                            </div>

                            <div class="game-summary">
                                <p>
                                    Axie Infinity Origins is a card-based strategy game where players collect, own, and
                                    use
                                    an infinite variety of creatures called Axies to battle. Players can take down evil
                                    Chimeras in the Adventure mode or challenge other players in the Arena to reach the
                                    top
                                    of the Leaderboard.
                                </p>
                            </div>
                        </div>

                        <div class="row flex-col mt-4 gap-3 system-requirements-section xl-mt-8 xl-col-9">

                            <div class="pt-2 pb-2 title-primary">
                                System Requirements
                            </div>

                            <div class="hardware-requirements">
                                <div class="pt-5 pr-5 pl-5 nav-tabs">
                                   ${navTabsList()}
                                </div>


                                <div class="pt-2">

                                ${systemRequirementsDetailData.map((item)=>{
                                    return systemRequirementsDetail({key: item.key, requirements: item.requirements})
                                }).join("")}
  
                                        
                                        
                                </div>
                            </div>

                        </div>

                    </div>

            </div>
            </section>
        </div>
        </div>
    </main>`;
};
