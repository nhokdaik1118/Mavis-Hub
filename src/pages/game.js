import { header } from "../components/index.js";
import { footer } from "../components/footer/index.js";
import { browseMain } from  "../components/browse-main/index.js"

const gamePage = () => {
  return `${header()}
  ${browseMain()}
  ${footer()}

`
;
};


export default gamePage;
