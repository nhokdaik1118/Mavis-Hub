import { header } from "../components/index.js";
import { footer } from "../components/footer/index.js";
import { productDetailMain } from "../components/product-detail-main/index.js";

const greenLightPage = () => {
  return `${header()}
  ${productDetailMain()}
  ${footer()}`;
};

export default greenLightPage;
