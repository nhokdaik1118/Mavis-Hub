
import { header } from "../components/index.js";
import { main } from "../components/main/index.js";
import { footer } from "../components/footer/index.js";

const homepage = () => {
  return `${header()}
    ${main()}
    ${footer()}

  `
  ;
};

export default homepage;
