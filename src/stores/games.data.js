export const mostPlayedGamesData = [
  {
    name: "Axie Infinity: Origins",
    description: "Turn-based, Strategy,Adventure",
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-thumbnail.png",
  },
  {
    name: " Axie Classic",
    description: "Turn-based, Strategy,Adventure",
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/classic-thumbnail.png",
  },
  {
    name: "Axie Infinity: Homeland",
    description: "Strategy, Adventure,Build & Battle",
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1706002032-game_thumbnail.png",
  },
];

export const titlePrimaryData = {
  featured: "Featured Games",
  most: "Most Played Games",
  highlight: "Highlight Games",
  news: "News & Events",
  popular: "Popular Genres",
};

export const highlightGamesData = [
  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1710172956-banner_wf.png",
    publisher: "Zillion Whales",
    name: "Wild Forest",
    platforms: ["windows", "chplay", "ios", "apple"],
    badges: ["Early access", "Strategy"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1709178744-gamethumbnail_564x376.jpg",
    publisher: "Foonie Magus",
    name: "Apeiron",
    platforms: ["windows", "apple"],
    badges: ["Strategy", "Adventure"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1706002032-game_thumbnail.png",
    publisher: "Sky Mavis",
    name: "Axie Infinity: Homeland",
    platforms: ["windows", "apple"],
    badges: ["Strategy", "Adventure", "+1"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1715055788-gamethumbnail.png",
    publisher: "Tribes Studio",
    name: "Tribesters - Keeper of Secrets",
    platforms: ["windows", "apple"],
    badges: ["Early access", "Adventure"],
  },

  {
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-thumbnail.png",
    publisher: "Sky Mavis",
    name: "Axie Infinity: Origins",
    platforms: ["windows", "apple", "ios", "android", "chplay"],
    badges: ["Turn-based", "Strategy"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1708488768-thumbnail_564x376.png",
    publisher: "Bali games",
    name: "Axie Champions",
    platforms: ["android"],
    badges: ["RPG", "Puzzle", "+1"],
  },
];

export const newsAndEventsData = [
  {
    img: "https://substackcdn.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fsubstack-post-media.s3.amazonaws.com%2Fpublic%2Fimages%2Fe6d8ede7-49c9-436d-9222-8f1e8e04c8ec_2400x1350.png",
    title: "The Homeland Adventurers’ Rush is Coming! ",
    date: "Jun 21, 2024 ",
  },

  {
    img: "https://substackcdn.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fsubstack-post-media.s3.amazonaws.com%2Fpublic%2Fimages%2F3b8862b4-691b-47c2-ac05-a141b9ab0c1a_1092x826.png",
    title: "Pixels Chapter 2 is LIVE! ",
    date: "Jun 19, 2024 ",
  },

  {
    img: "https://substackcdn.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fsubstack-post-media.s3.amazonaws.com%2Fpublic%2Fimages%2F124c391c-f1e3-4b9b-9854-517df8a28836_3200x1800.png",
    title: "Introducing: Ronin ZkEVM ",
    date: "Jun 18, 2024 ",
  },

  {
    img: "https://substackcdn.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fsubstack-post-media.s3.amazonaws.com%2Fpublic%2Fimages%2F7055b45e-464b-4d1f-884e-145ae2a16558_3000x1688.png",
    title: "Origins S9 Streamer Race ",
    date: " Jun 13, 2024",
  },
];

export const popularGenresData = [
  {
    title: "Adventure",
    icon: "adventure",
  },

  {
    title: "Puzzle",
    icon: "puzzle",
  },

  {
    title: "RPG",
    icon: "rpg",
  },

  {
    title: "Strategy",
    icon: "strategy",
  },

  {
    title: "Turn-Based",
    icon: "turn",
  },

  {
    title: "Build & Battle",
    icon: "build",
  },
];

export const footerData = [
  {
    title: "Discover",
    type: "text",
    items: [
      {
        text: "Browse",
        url: "#",
      },
      {
        text: "Greenlight",
        url: "#",
      },
      {
        text: "App.axie",
        url: "#",
      },
      {
        text: "Mavis Market",
        url: "#",
      },
    ],
  },
  {
    title: "Community",
    type: "text",
    items: [
      {
        text: "Blog",
        url: "#",
      },
      {
        text: "Help Center",
        url: "#",
      },
    ],
  },
  {
    title: "Join us",
    type: "icon",
    items: [
      {
        text: "discord",
        url: "#",
      },
      {
        text: "twitter",
        url: "#",
      },
      {
        text: "facebook",
        url: "#",
      },
    ],
  },
];

export const filtersOptionData = [
  {
    title: "PARTNERSHIP",
    description: [{
      text: "Sky Mavis Games",
    },
    {
      text: "Axie Builder Program",
    },
    {
      text: "Studio Partnership",
    },
    {
      text: "Community Developer",
    },
    ]
  },

  {
    title: "GAME GENRE",
    description: [
      {
        text: "Early access",
      },
      {
        text: "Turn-based",
      },
      {
        text: "Strategy",
      },
      {
        text: "Adventure",
      },
      {
        text: "RPG",
      },
      {
        text: "MMORPG",
      },
      {
        text: "Action RPG",
      },
      {
        text: "4x",
      },
      {
        text: "Auto Battler",
      },
      {
        text: "Battle Royale",
      },
      {
        text: "Puzzle",
      },
      {
        text: "Match-3",
      },
      {
        text: "Merge",
      },
      {
        text: "Hypercasual",
      },
      {
        text: "Sports",
      },
      {
        text: "Racing",
      },
      {
        text: "Fighting",
      },
      {
        text: "Simulation",
      },
      {
        text: "Survival",
      },
      {
        text: "Build & Battle",
      },
      {
        text: "Music",
      },

    ]
  },

  {
    title: "PLATFORM",
    description: [
      {
        text: "Windows",
      },
      {
        text: "MacOS",
      },
      {
        text: "Android APK",
      },
      {
        text: "Android",
      },
      {
        text: "iOS",
      },
      {
        text: "Web",
      },
    ]
  }
]

export const browseGameCardData = [
  {
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-thumbnail.png",
    name: "Axie Infinity: Origins",
    badges: ["Turn-based", "Strategy", "+ 1"],
  },
  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1706002032-game_thumbnail.png",
    name: "Axie Infinity: Homeland",
    badges: ["Strategy", "Adventure", "+ 1"],
  },

  {
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/classic-thumbnail.png",
    name: "Axie Classic",
    badges: ["Turn-based", "Strategy"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1688981162-raylights-youtube-thumbnail.jpg",
    name: "Axie Infinity: Raylights",
    badges: ["Puzzle"],
  },

  {
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/doll-banner.jpeg",
    name: "Defenders of Lunacian Land",
    badges: ["Action RPG", "Survival"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1692030936-4.jpg",
    name: "Project T prototype",
    badges: ["Adventure", "RPG"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1709178744-gamethumbnail_564x376.jpg",
    name: "Apeiron",
    badges: ["Strategy", "Adventure"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1708488768-thumbnail_564x376.png",
    name: "Axie Champions",
    badges: ["RPG", "Puzzle", "+ 1"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1710172956-banner_wf.png",
    name: "Wild Forest",
    badges: ["Early access", "Strategy"],
  },


  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1713172492-puffgobanner-564_376.png",
    name: "Puffgo",
    badges: ["Early access", "Battle Royale"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1714645512-feature_808x500.png",
    name: "Duet Monsters",
    badges: ["Hypercasual", "Music"],
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1715055788-gamethumbnail.png",
    name: "Tribesters - Keeper of Secrets",
    badges: ["Early access", "Adventure"],
  },

];

export const thumbnailListDataDetail = [
  {
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-banner-key-art.png"
  },

  {
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-banner-trailer-thumbnail.png"
  },

  {
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-banner-fight.png"
  },

  {
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-banner-cards.png"
  },

  {
    img: "https://cdn.axieinfinity.com/marketplace-website/banner/game/origins-banner-battle.png"
  },
]

export const badgeData = [
  {
    badges: "Turn-based"
  },

  {
    badges: "Strategy"
  },

  {
    badges: "Adventure"
  },
]

export const collectionData = [
  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1696575551-tab-axie.png"
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1696575529-tab-rune.png"
  },

  {
    img: "https://storage.googleapis.com/sm-prod-mavis-hub-cdn/assets/1696575503-tab-charm.png"
  },
]


export const systemRequirementsDetailData = [
  {
    key: "Windows",
    requirements: [
      {
        title: "MINIMUM",
        specName:
        {
          'Os': 'Windows 7/8/8.1/10 (64-bit versions)',

          'Processor': 'Intel Core i5/i7/i9/Xeon/Ryzen/Athlon/Phenom/Athlon XP/Athlon MP/Athton XP/Athlon MP',

          'Memory': '8 GB RAM',

          'Graphics': 'NVIDIA GTX 670 2GB / AMD Radeon HD7870 wGB or better',

          'Storage': '500 GB available space',

          'Additional Notes': ''
        }
      },

      {
        title: "RECOMMENDED",
        specName:
        {
          'Os': 'Windows 7/8/8.1/10 (64-bit versions)',

          'Processor': 'Intel Core i5/i7/i9/Xeon/Ryzen/Athlon/Phenom/Athlon XP/Athlon MP/Athton XP/Athlon MP',

          'Memory': '8 GB RAM',

          'Graphics': 'NVIDIA GTX 670 2GB / AMD Radeon HD7870 wGB or better',

          'Storage': '500 GB available space',

          'Additional Notes': ''
        }
      }
    ]
  },

  {
    key:"MacOS",
    requirements:[
      {
        title: "MINIMUM",
        specName:
        {
          'Os': 'OSX 10.9 or Higher',
    
          'Processor': '2 GHz Equivalent CPU',
    
          'Memory': '4000 MB RAM',
    
          'Graphics': 'OpenGL 3 Compatible GPU with 1GB Video RAM',
    
          'Storage': '20000 MB available space',
    
          'Additional Notes': ''
        }
      },
    
      {
        title: "RECOMMENDED",
        specName:
        {
          'Os': 'OSX 10.9 or Higher',
    
          'Processor': '2 GHz Equivalent CPU',
    
          'Memory': '4000 MB RAM',
    
          'Graphics': 'OpenGL 3 Compatible GPU with 1GB Video RAM',
    
          'Storage': '20000 MB available space',
    
          'Additional Notes': ''
        }
      }
    ]
  },

  {
    key:"iOS",
    requirements: [
      {
        title: "MINIMUM",
        specName:
        {
          'Os': '',
    
          'Processor': '',
    
          'Memory': '',
    
          'Graphics': '',
    
          'Storage': '',
    
          'Additional Notes': ''
        }
      },
    
      {
        title: "RECOMMENDED",
        specName:
        {
          'Os': 'iOS 9.0 or later',
    
          'Processor': '',
    
          'Memory': '',
    
          'Graphics': '',
    
          'Storage': '20000 MB available space',
    
          'Additional Notes': ''
        }
      }
    ]
  },

  {
    key: "Android APK",
    requirements: [
      {
        title: "MINIMUM",
        specName:
        {
          'Os': '',
    
          'Processor': '',
    
          'Memory': '',
    
          'Graphics': '',
    
          'Storage': '',
    
          'Additional Notes': ''
        }
      },
    
      {
        title: "RECOMMENDED",
        specName:
        {
          'Os': 'iOS 10.0 or later',
    
          'Processor': '',
    
          'Memory': '',
    
          'Graphics': '',
    
          'Storage': '20000 MB available space',
    
          'Additional Notes': ''
        }
      }
    ]
  },

  {
    key:"Android",
    requirements: [
      {
        title: "MINIMUM",
        specName:
        {
          'Os': '',
    
          'Processor': '',
    
          'Memory': '',
    
          'Graphics': '',
    
          'Storage': '',
    
          'Additional Notes': ''
        }
      },
    
      {
        title: "RECOMMENDED",
        specName:
        {
          'Os': '',
    
          'Processor': '',
    
          'Memory': '',
    
          'Graphics': '',
    
          'Storage': '',
    
          'Additional Notes': ''
        }
      }
    ]
  }
]

export const navTabsData = [
  {
    text: "Windows"
  },

  {
    text: "MacOS"
  },
  {
    text: "iOS"
  },
  {
    text: "Android APK"
  },
  {
    text: "Android"
  },
]

export const productDetailData = [
  {
    title: "PLATFORM",
    platforms: ["windows", "apple", "ios", "android", "chplay"],
  },
  {
    title: "DEVELOPED BY",
    text: "Sky Mavis"
  }
]

export const buttonDetailData = [
  {
    title: "Install",
  },

  {
    title: "Play on mobile",
    icon: "mobile"
  },

  {
    title: " Download APK",
    icon: "download"
  },

  {
    title: "Share with friends",
    icon: "share"
  },
]


