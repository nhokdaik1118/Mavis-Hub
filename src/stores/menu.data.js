import { currentURL } from "../utils/index.js";

export const menuData = [
  {
    name: "Home",
    url: "/",
    icon: ` <svg viewBox="0 0 24 24" width="20" height="20" style="fill: currentcolor;">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M21 10.15v7.817C21 20.194 19.21 22 17 22H7c-2.21 0-4-1.806-4-4.033V10.15c0-1.21.54-2.357 1.47-3.123l5-4.118a3.975 3.975 0 0 1 5.06 0l5 4.118A4.046 4.046 0 0 1 21 10.15Zm-5.75 7.1v2.25a1 1 0 0 1-1 1h-4.5a1 1 0 0 1-1-1v-2.25a3.25 3.25 0 0 1 6.5 0Z"
                        fill="currentColor"></path>
            </svg>`,
  },
  {
    name: "Browse",
    url: `${currentURL()}?page=games`,
    icon: `<svg viewBox="0 0 24 24" width="20" height="20" style="fill: currentcolor;">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M22 18.323V8.726c0-.22-.018-.439-.054-.653H2.054A4 4 0 0 0 2 8.726v9.597a4 4 0 0 0 4 4h12a4 4 0 0 0 4-4Zm-.877-12.096c.09.111.172.227.248.346H2.63c.076-.12.159-.235.248-.346l1.922-2.403a4 4 0 0 1 3.123-1.501h8.156A4 4 0 0 1 19.2 3.824l1.922 2.403ZM9.75 14.323a2.25 2.25 0 1 1 4.5 0 2.25 2.25 0 0 1-4.5 0Zm2.25-3.75a3.75 3.75 0 1 0 2.068 6.878l.902.902a.75.75 0 1 0 1.06-1.06l-.901-.902A3.75 3.75 0 0 0 12 10.573Z"
                        fill="currentColor"></path>
                </svg>`,
  },
  {
    name: "Community",
    url: `${currentURL()}?page=greenlight`,
    icon: ` <svg viewBox="0 0 24 24" width="20" height="20" style="fill: currentcolor;">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M12 11a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm0 8c3.314 0 6-1.343 6-3s-2.686-3-6-3-6 1.343-6 3 2.686 3 6 3Zm-5.618-5.981C3.912 13.171 2 14.224 2 15.5c0 1.052 1.3 1.952 3.14 2.321-.412-.556-.64-1.173-.64-1.82 0-1.144.71-2.188 1.882-2.982ZM19.5 16c0 .648-.228 1.265-.64 1.821 1.84-.369 3.14-1.269 3.14-2.32 0-1.277-1.912-2.33-4.382-2.482C18.79 13.813 19.5 14.857 19.5 16Zm-3.782-5.465a4.518 4.518 0 0 0 .69-3.446 2 2 0 1 1-.69 3.446ZM7 7a2 2 0 0 1 .592.09 4.52 4.52 0 0 0 .69 3.445A2 2 0 1 1 7 7Z"
                        fill="currentColor"></path>
                </svg>`,
  },
  {
    name: "Event",
    url: "/event",
    icon: `  <svg viewBox="0 0 24 24" width="20" height="20" style="fill: currentcolor;">
                    <path
                        d="M8 1.25a.75.75 0 0 1 .75.75v1.5h6.5V2a.75.75 0 0 1 1.5 0v1.5H17a4 4 0 0 1 4 4v.75H3V7.5a4 4 0 0 1 4-4h.25V2A.75.75 0 0 1 8 1.25Z"
                        fill="currentColor"></path>
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M21 9.75H3V18a4 4 0 0 0 4 4h10a4 4 0 0 0 4-4V9.75Zm-8.312 2.695a.755.755 0 0 0-1.377 0l-.486 1.025a.77.77 0 0 1-.578.437l-1.089.165c-.63.095-.881.9-.425 1.361l.787.798a.818.818 0 0 1 .221.707l-.186 1.127c-.107.652.55 1.15 1.114.841l.974-.532a.742.742 0 0 1 .714 0l.974.533c.563.307 1.221-.19 1.114-.842l-.186-1.127a.818.818 0 0 1 .22-.707l.788-.798c.456-.462.205-1.266-.425-1.361l-1.089-.165a.77.77 0 0 1-.578-.437l-.487-1.025Z"
                        fill="currentColor"></path>
                </svg>`,
  },
];
