export const navigate = (page) => {
  const urlParams = new URLSearchParams(window.location.search);
  urlParams.set("page", page);
  window.history.pushState(
    {},
    "",
    `${window.location.pathname}?${urlParams.toString()}`
  );
};

export const currentURL = () => {
  return window.location.origin;
};
